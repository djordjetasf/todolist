var item_id = 1;

//Event handler kad se stisne +
function add_new() {
    const form = document.getElementById('input-form');
    form.classList.toggle('cont_crear_new');
    form.classList.toggle('cont_crear_new_active');
}

//Event handler kad se stine ADD dugme
function add_to_list() {

    var action_element = document.getElementById("action_select");

    var title_element = document.getElementById('title_val');
    var description_element = document.getElementById('description_val');

    var date_element = document.getElementById("date_select");

    if (!validate(action_element, title_element, description_element, date_element)) {
        return;
    }

    var action = action_element.options[action_element.selectedIndex].value;
    var date = date_element.options[date_element.selectedIndex].value;

    add_item_to_dom(item_id, action, title_element.value, description_element.value, date);
    item_id++;
}

function validate(action, title, description, date) {
    var result = true;

    //vracamo na normalu
    remove_all_invalids(action, title, description, date);

    if (action.selectedIndex < 0) {
        set_input_invalid(action)
        result = false;
    }

    if (title.value == '') {
        set_input_invalid(title);
        result = false;
    }

    if (description.value == '') {
        set_input_invalid(description);
        result = false;
    }

    if (date.selectedIndex < 0) {
        set_input_invalid(date)
        result = false;
    }

    return result;
}

//Resetuje validaciju za sve inpute
function remove_all_invalids(action, title, description, date) {
    const invalid_class = 'invalid';

    action.classList.remove(invalid_class);
    title.classList.remove(invalid_class);
    description.classList.remove(invalid_class);
    date.classList.remove(invalid_class);
}

function set_input_invalid(input) {
    input.classList.add('invalid');
}


function add_item_to_dom(id, ac, tit, desc, dt) {
    var ul_items = document.getElementById('items_ul');

    var new_li = document.createElement('li');
    var new_li_class = 'list_' + ac;
    new_li.classList.add(new_li_class);
    new_li.classList.add('li_num_0_1');
    new_li.setAttribute('id', 'li' + id);


    var div1 = document.createElement('div');
    div1.classList.add('col_md_1_list');

    var par1 = document.createElement('p');
    var par1_text = document.createTextNode(ac);
    par1.appendChild(par1_text);

    div1.appendChild(par1);

    var div2 = document.createElement('div');
    div2.classList.add('col_md_2_list');

    var div2_h4 = document.createElement('h4');
    var div2_h4_text = document.createTextNode(tit);
    div2_h4.appendChild(div2_h4_text);

    var div2_p = document.createElement('p');
    var div2_p_text = document.createTextNode(desc);
    div2_p.appendChild(div2_p_text);

    div2.appendChild(div2_h4);
    div2.appendChild(div2_p);



    var div3 = document.createElement('div');
    div3.classList.add('col_md_3_list');

    var div3_div1 = document.createElement('div');
    div3_div1.classList.add('cont_text_date');

    var div3_div_p = document.createElement('p');
    var div3_div_p_text = document.createTextNode(dt);
    div3_div_p.appendChild(div3_div_p_text);

    div3_div1.appendChild(div3_div_p);


    var div3_div2 = document.createElement('div');
    div3_div2.classList.add('cont_btns_options');

    var div4_ul = document.createElement('ul');
    var div4_ul_li = document.createElement('li');

    var div4_ul_li_a = document.createElement('a');
    div4_ul_li_a.setAttribute('href', '#');
    div4_ul_li_a.setAttribute('onclick', 'finish_action(' + id + ');');

    var div4_ul_li_a_i = document.createElement('i');
    div4_ul_li_a_i.classList.add('material-icons');

    var i_text = document.createTextNode('');
    div4_ul_li_a_i.appendChild(i_text);

    div4_ul_li_a.appendChild(div4_ul_li_a_i);
    div4_ul_li.appendChild(div4_ul_li_a);
    div4_ul.appendChild(div4_ul_li);
    div3_div2.appendChild(div4_ul);

    div3.appendChild(div3_div1);
    div3.appendChild(div3_div2);

    new_li.appendChild(div1);
    new_li.appendChild(div2);
    new_li.appendChild(div3);

    ul_items.appendChild(new_li);
}

function finish_action(li_id) {
    var element_to_remove = document.getElementById('li' + li_id);
    if (element_to_remove != null) {
        document.getElementById('items_ul').removeChild(element_to_remove);
    }
}